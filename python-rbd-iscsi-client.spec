%global _empty_manifest_terminate_build 0
Name:           python-rbd-iscsi-client
Version:        0.1.8
Release:        1
Summary:        "REST client to talk to rbd-target-api"
License:        Apache-2.0
URL:            http://github.com/hemna/rbd-iscsi-client
Source0:        https://files.pythonhosted.org/packages/83/61/e301f9ebe3317779f95725cd3f17cc013f970314973ef2d6616c60450183/rbd-iscsi-client-0.1.8.tar.gz
BuildArch:      noarch
%description
 RBD iSCSI Client This is a REST client that talks to ceph-iscsi's rbd-target-
api to export rbd images/volumes to an iSCSI initiator.

%package -n python3-rbd-iscsi-client
Summary:        "REST client to talk to rbd-target-api"
Provides:       python-rbd-iscsi-client
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-babel
BuildRequires:  python3-oslo-i18n
BuildRequires:  python3-oslo-log
BuildRequires:  python3-oslo-utils
BuildRequires:  python3-requests
BuildRequires:  python3-six
BuildRequires:  python3-stestr
BuildRequires:  python3-pycodestyle
BuildRequires:  python3-testscenarios
BuildRequires:  python3-oslotest
BuildRequires:  python3-openstackdocstheme
BuildRequires:  python3-sphinx
BuildRequires:  python3-reno
BuildRequires:  python3-ddt
BuildRequires:  python3-coverage
BuildRequires:  python3-flake8-logging-format
BuildRequires:  python3-flake8-import-order
# General requires
Requires:       python3-babel
Requires:       python3-oslo-i18n
Requires:       python3-oslo-log
Requires:       python3-oslo-utils
Requires:       python3-pbr
Requires:       python3-requests
Requires:       python3-six
Requires:       python3-stestr
%description -n python3-rbd-iscsi-client
 RBD iSCSI Client This is a REST client that talks to ceph-iscsi's rbd-target-
api to export rbd images/volumes to an iSCSI initiator.

%package help
Summary:        "REST client to talk to rbd-target-api"
Provides:       python3-rbd-iscsi-client-doc
%description help
 RBD iSCSI Client This is a REST client that talks to ceph-iscsi's rbd-target-
api to export rbd images/volumes to an iSCSI initiator.

%prep
%autosetup -n rbd-iscsi-client-0.1.8

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-rbd-iscsi-client -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Jul 13 2021 OpenStack_SIG <openstack@openeuler.org> - 0.1.8-1
- Package Spec generate
